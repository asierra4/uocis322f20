Proj0-Hello
-------------

Simple program that prints out "Hello world" once it is ran 
using the appropriate Makefile, and corresponding commands.

# Author:
---------------

Author of this simple program is Adrian Sierra, a fourth year CIS undergraduate.

# Contact Address:
----------------

If issues were to arise from attempting to run this program, you can
contact the author at asierra4@uoregon.edu, or personal cell number
5034001609
